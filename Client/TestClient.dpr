program TestClient;

uses
  System.StartUpCopy,
  FMX.Forms,
  frmMain in 'forms\frmMain.pas' {Form2},
  AmCtrl in 'modules\AmCtrl.pas' {AmController: TDataModule};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := true;
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
