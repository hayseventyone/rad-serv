unit AmCtrl;

interface

uses
  System.SysUtils, System.Classes, REST.Types, REST.Client,
  Data.Bind.Components, Data.Bind.ObjectScope, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP;

type
  TAmController = class(TDataModule)
    IdHTTP1: TIdHTTP;
    RestClient: TRESTClient;
    RESTRequest: TRESTRequest;
    RESTResponse: TRESTResponse;
  private
    { Private declarations }
  public
    { Public declarations }
    function Fetch(): String;
    function SendPostData(): String;
  end;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}
{$R *.dfm}
{ TAmController }

function TAmController.Fetch: String;
var
  lHTTP: TIdHTTP;
begin
  try
    lHTTP := TIdHTTP.Create(nil);

    result := lHTTP.Get('http://localhost:8080/test');
  finally
    lHTTP.Free;
  end;
end;

function TAmController.SendPostData: String;
var
  lHTTP: TIdHTTP;
  sResponse: String;
  fsParams: TFileStream;
begin

  try
    fsParams := TFileStream.Create('.\test.xml', fmOpenRead or fmShareDenyWrite);
    lHTTP := TIdHTTP.Create(nil);
    try

      lHTTP.Request.ContentType := 'text/xml';
      sResponse := lHTTP.Post('http://localhost:8080/test', fsParams);
      result := (sResponse);
    except
      on E: Exception do
        result := ('Error encountered during POST: ' + E.Message);
    end;

  finally
    lHTTP.Free;
    fsParams.Free;
  end;

end;

end.
