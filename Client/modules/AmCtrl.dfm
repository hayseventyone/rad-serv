object AmController: TAmController
  OldCreateOrder = False
  Height = 330
  Width = 503
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 328
    Top = 112
  end
  object RestClient: TRESTClient
    Accept = 'application/json, text/plain; q=0.9, text/html;q=0.8,'
    AcceptCharset = 'utf-8, *;q=0.8'
    BaseURL = 'http://localhost:8080/'
    Params = <>
    Left = 64
    Top = 40
  end
  object RESTRequest: TRESTRequest
    Client = RestClient
    Params = <>
    Resource = 'test'
    Response = RESTResponse
    SynchronizedEvents = False
    Left = 152
    Top = 40
  end
  object RESTResponse: TRESTResponse
    ContentType = 'text/html'
    Left = 64
    Top = 96
  end
end
