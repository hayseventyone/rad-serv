unit frmMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, REST.Types,
  REST.Client, Data.Bind.Components, Data.Bind.ObjectScope, FMX.ScrollBox,
  FMX.Memo, FMX.Controls.Presentation, FMX.StdCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  AmCtrl;

type
  TForm2 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    btnPost: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnPostClick(Sender: TObject);
  private
    { Private declarations }
    FController: TAmController;
    procedure createController();
    procedure GetData();
    procedure CheckController();
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
begin
  createController();
end;

procedure TForm2.CheckController;
begin
  if FController = nil then
    Abort;
end;

procedure TForm2.createController;
begin
  if not Assigned(FController) then
    FController := TAmController.Create(Self);
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  if Form2 = Self then
    Form2 := nil
end;

procedure TForm2.btnPostClick(Sender: TObject);
begin
  CheckController;
  Memo1.Text :=  FController.SendPostData
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  GetData();
end;

procedure TForm2.GetData;
begin
   CheckController;
   Memo1.Text :=  FController.Fetch();
end;

end.
